# Analog Output Audio Mixer

This is an audio mixer synth module in Kosmo format. It has three inputs, each with a slide attenuator, and a general level control and two identical outputs.

## Current draw
4 mA +12 V, 4 mA -12 V

## Photos

![](Images/IMG_6735.JPG)
![](Images/IMG_6744.JPG)
![](Images/IMG_6747.JPG)

## Documentation:

* [Schematic](Docs/ao_audio_mixer.pdf)
* [PCB layout](Docs/ao_audio_mixer_layout.pdf)
* [BOM](Docs/ao_audio_mixer_bom.md)
* [Build notes](Docs/build.md)

## Git repository

* [https://gitlab.com/rsholmes/audiomixer](https://gitlab.com/rsholmes/audiomixer)


